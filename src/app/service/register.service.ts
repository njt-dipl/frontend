import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL } from '../app.constants';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {


  constructor(private http: HttpClient) { }


  saveUser(username: string, password: string, firstname: string, lastname: string, email: string, creationDate: Date) {
    return this.http.post<any>(
      `${API_URL}/save`, {
      username,
      password, firstname, lastname, email, creationDate
    })
  }
}

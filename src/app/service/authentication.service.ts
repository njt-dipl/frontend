import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { API_URL } from '../app.constants';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { User } from '../component/login/login.component';



export const TOKEN = 'token';
export const AUTHENTICATED_USER = 'authenticatedUser';




@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
userLoggedIn = new EventEmitter<User>();


  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  executeJWTAuthenticationService(username: string, password: string) {
    return this.http.post<any>(
      `${API_URL}/authenticate`, {
      username,
      password
    }).pipe(
      map(
        data => {
          sessionStorage.setItem(AUTHENTICATED_USER, username);
          sessionStorage.setItem(TOKEN, `Bearer ${data.token}`);
          console.log(data)
          // sessionStorage.setItem(USER, JSON.stringify(user));
          return data;
        }
      )
    );
  }



  isUserLoggedIn() {
    let user = sessionStorage.getItem(AUTHENTICATED_USER)
    return !(user === null)
  }

  logout() {
    sessionStorage.removeItem(AUTHENTICATED_USER)
    sessionStorage.removeItem(TOKEN)
    localStorage.removeItem('loggedUser');
    this.router.navigate(['login']);
  }
  getAuthenticatedUser() {
    return sessionStorage.getItem(AUTHENTICATED_USER)
  }

  getAuthenticatedToken() {
    if (this.getAuthenticatedUser())
      return sessionStorage.getItem(TOKEN)
    return null;
  }

  getUser(username: string) {
    return this.http.get<User>(`${API_URL}/${username}`)
  }
}

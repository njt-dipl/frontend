import { TestBed } from '@angular/core/testing';

import { UserPassingService } from './user-passing.service';

describe('UserPassingService', () => {
  let service: UserPassingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserPassingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

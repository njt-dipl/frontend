import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL } from '../app.constants';


@Injectable({
  providedIn: 'root'
})
export class MessageService {



  constructor(private http: HttpClient) { }

  sendMail(email: String, username: String, password: String) {
    return this.http.post<any>(
      `${API_URL}/signup-success`, {
      email, username, password
    })
  }
}
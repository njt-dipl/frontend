import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter, OnChanges } from '@angular/core';
import { API_URL } from '../app.constants';
import { User } from '../component/login/login.component';

@Injectable({
  providedIn: 'root'
})
export class UserPassingService implements OnChanges {
 

 userLoggedIn = new EventEmitter<User>();
 user1 = new User(-1, 'first', 'u servisu', 'u servisu', 'u servisu', 'u servisu');

  constructor(private http: HttpClient) { }

  ngOnChanges() {
    console.log('********************************');
    console.log(this.userLoggedIn);
  }

  emitUser(user: User) {
    this.userLoggedIn.emit()
    // this.user1 = user;
  }

  getEmiiter(){
    return this.userLoggedIn;
  }

  update(userLoggedIn: User) {
    return this.http.put(`${API_URL}/update`, userLoggedIn);

  }

}

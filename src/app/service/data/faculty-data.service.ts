import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL } from 'src/app/app.constants';
import { City, Faculty, FacultyGrouping, StudyModule, StudyProgram, University } from 'src/app/component/faculty/faculty.component';

@Injectable({
  providedIn: 'root'
})
export class FacultyDataService {

  constructor(
    private http: HttpClient
  ) { }

  getFaculty(facultyId: number) {
    return this.http.get<Faculty>(`${API_URL}/faculty/${facultyId}`)
  }

  getAllUniversities() {
    return this.http.get<University[]>(`${API_URL}/university/getAll`)
  }

  getAllFacultyGroupings() {
    return this.http.get<FacultyGrouping[]>(`${API_URL}/facultyGrouping/getAll`)
  }

  getAllCities() {
    return this.http.get<City[]>(`${API_URL}/city/getAll`)
  }

  getAllFaculties() {
    return this.http.get<Faculty[]>(`${API_URL}/faculty/getAll`)
  }

  getModulesByStudyProgram(idStudyProgram: number) {
    return this.http.get<StudyModule[]>(`${API_URL}/studyModule/getByStudyProgram/${idStudyProgram}`)
  }
  
  getStudyProgramsByFaculty(facultyId: number) {
    return this.http.get<StudyProgram[]>(`${API_URL}/studyProgram/getByFaculty/${facultyId}`)
  }

  getModules() {
    return this.http.get<StudyModule[]>(`${API_URL}/studyModule/getAll`)
  }

  getStudyModule(studyModuleId: number) {
    return this.http.get<StudyModule>(`${API_URL}/studyModule/${studyModuleId}`)
  }

  getIdModuleForSP(idStudyProgram: number) {
    return this.http.get<StudyModule>(`${API_URL}/studyModule/studyProgram/${idStudyProgram}`)
  }
}

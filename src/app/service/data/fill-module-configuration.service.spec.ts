import { TestBed } from '@angular/core/testing';

import { FillModuleConfigurationService } from './fill-module-configuration.service';

describe('FillModuleConfigurationService', () => {
  let service: FillModuleConfigurationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FillModuleConfigurationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

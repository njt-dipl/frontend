import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL } from 'src/app/app.constants';
import { FundOfClasses, TeachingForm } from 'src/app/component/fund-of-classes/fund-of-classes.component';
import { Subject, SubjectType } from 'src/app/component/subject/subject.component';

@Injectable({
  providedIn: 'root'
})
export class SubjectDataService {

  constructor(
    private http: HttpClient
  ) { }

  getSubject(facultyId: number, subjectId: number) {
    return this.http.get<Subject>(`${API_URL}/subject/getAll/${facultyId}/${subjectId}`)
  }

  getSubjectsForFaculty(facultyId: number) {
    return this.http.get<Subject[]>(`${API_URL}/subject/getAll/${facultyId}`)
  }

  createSubject(subject: Subject) {
    return this.http.post<Subject>(`${API_URL}/subject/save`, subject);
  }

  updateSubject(subjectId: number, subject: Subject) {
    return this.http.put(`${API_URL}/subject/getAll/${subject.facultyId}/${subjectId}`, subject);
  }

  deleteSubject(subjectId: number) {
    return this.http.delete(`${API_URL}/subject/delete/${subjectId}`);
  }

  getSubjectTypes() {
    return this.http.get<SubjectType[]>(`${API_URL}/subjectType/getAll`)
  }

  // getSubjectOfType(idType: number) {
  //   return this.http.get<Subject[]>(`${API_URL}/subject/getAll/type/${idType}`)
  // }

  createClassesFund(subjectId: number, teachingFormId: number, numberOfClasses: number) {
    return this.http.post<FundOfClasses>(`${API_URL}/fundOfClasses/save`, { subjectId, teachingFormId, numberOfClasses });
  }

  getTeachingForms() {
    return this.http.get<TeachingForm[]>(`${API_URL}/teachingForm/getAll`)
  }

  getFunds() {
    return this.http.get<FundOfClasses[]>(`${API_URL}/fundOfClasses/getAll`)
  }

  getFundsOfSubject(subjectId: number) {
    return this.http.get<FundOfClasses[]>(`${API_URL}/fundOfClasses/getAll/${subjectId}`)
  }

  deleteFund(subjectId: number, teachingFormId: number) {
    console.log("u servisu je")
    return this.http.delete(`${API_URL}/fundOfClasses/delete/${subjectId}/${teachingFormId}`)

  }


}

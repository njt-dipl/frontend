import { TestBed } from '@angular/core/testing';

import { SemesterConfigurationService } from './semester-configuration.service';

describe('SemesterConfigurationService', () => {
  let service: SemesterConfigurationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SemesterConfigurationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

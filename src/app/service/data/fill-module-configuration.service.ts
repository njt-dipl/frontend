import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL } from 'src/app/app.constants';
import { SubjectConfiguration } from 'src/app/component/fill-module-configuration/fill-module-configuration.component';


@Injectable({
  providedIn: 'root'
})
export class FillModuleConfigurationService {

  constructor(private http: HttpClient) { }

  saveSubjectConfiguration(subjectConf: SubjectConfiguration) {
    console.log("SERVIS:",subjectConf);
    return this.http.post<SubjectConfiguration>(
      `${API_URL}/subjectConfiguration/save`, subjectConf)
  }

}

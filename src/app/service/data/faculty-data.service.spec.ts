import { TestBed } from '@angular/core/testing';

import { FacultyDataService } from './faculty-data.service';

describe('FacultyDataService', () => {
  let service: FacultyDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FacultyDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL } from 'src/app/app.constants';
import { SubjectConfiguration } from 'src/app/component/fill-module-configuration/fill-module-configuration.component';
import { SemesterConfiguration } from 'src/app/component/study-module-configuration/study-module-configuration.component';

@Injectable({
  providedIn: 'root'
})
export class SemesterConfigurationService {

  constructor(private http: HttpClient) { }

  saveConfiguration(idSemesterConfiguration: number, idStudyProgram: number, espb: number, position: number, semester: number, idSubjectType: number, mandatory: String, idStudyModule: number, status: boolean, subjectTypeName: String) {
    return this.http.post<SemesterConfiguration>(
      `${API_URL}/semesterConfiguration/save`, {
      idSemesterConfiguration, idStudyProgram, espb, position, semester, idSubjectType, mandatory, idStudyModule, status, subjectTypeName
    })
  }
  getSemesterConfigurationListByStudyModule(studyProgramId: number, studyModuleId: number, numberOfSemester: number) {
    return this.http.get<SemesterConfiguration[]>(`${API_URL}/semesterConfiguration/getByModule/${studyProgramId}/${studyModuleId}/${numberOfSemester}`)

  }

  getSemesterConfigurationListByStudyModuleAndSemester(studyProgramId: number, studyModuleId: number, numberOfSemester: number) {
    return this.http.get<SemesterConfiguration[]>(`${API_URL}/semesterConfiguration/getByModuleAndSemester/${studyProgramId}/${studyModuleId}/${numberOfSemester}`)
  }

  getSemesterConfigurationListByStudyProgramAndSemester(idStudyProgram: number, numberOfSemester: number) {
    return this.http.get<SemesterConfiguration[]>(`${API_URL}/semesterConfiguration/getByProgramAndSemester/${idStudyProgram}/${numberOfSemester}`)
  }

  getMandatories() {
    return this.http.get<String[]>(`${API_URL}/mandatory/getAll`)
  }

  deleteConfiguration(idSemesterConfiguration: number) {
    console.log(idSemesterConfiguration)
    return this.http.delete(`${API_URL}/semesterConfiguration/delete/${idSemesterConfiguration}`);

  }


  getSubjectConfigurationListByStudyModule(idStudyModule: number) {
    return this.http.get<SubjectConfiguration[]>(`${API_URL}/subjectConfiguration/getByModule/${idStudyModule}`)
  }
}
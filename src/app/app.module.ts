import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './component/login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HomeComponent } from './component/home/home.component';
import { RegisterComponent } from './component/register/register.component';
import { FooterComponent } from './component/footer/footer.component';
import { MenuComponent } from './component/menu/menu.component';
import { HttpIntercepterService } from './service/http/http-intercepter.service';
import { FacultyComponent } from './component/faculty/faculty.component';
import { SubjectComponent } from './component/subject/subject.component';
import { SubjectListComponent } from './component/subject-list/subject-list.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { StudyModuleConfigurationComponent } from './component/study-module-configuration/study-module-configuration.component';
import { FillModuleConfigurationComponent } from './component/fill-module-configuration/fill-module-configuration.component';
import { FundOfClassesComponent } from './component/fund-of-classes/fund-of-classes.component';
import { SearchFilterSubjectPipe } from './pipe/search-filter-subject.pipe';
import { SearchFilterConfigurationPipe } from './pipe/search-filter-configuration.pipe';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { NgSelectModule } from '@ng-select/ng-select';
import { UserPassingService } from './service/user-passing.service';
import { SettingsComponent } from './component/settings/settings.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { CommonModule } from '@angular/common';
// import { WjGridModule } from 'wijmo/wijmo.angular2.grid';
// import { WjGridFilterModule } from 'wijmo/wijmo.angular2.grid.filter';
// import { WjInputModule } from 'wijmo/wijmo.angular2.input';
// import { WjGridDetailModule } from "wijmo/wijmo.angular2.grid.detail";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    FooterComponent,
    MenuComponent,
    FacultyComponent,
    SubjectComponent,
    SubjectListComponent,
    StudyModuleConfigurationComponent,
    FillModuleConfigurationComponent,
    FundOfClassesComponent,
    SearchFilterSubjectPipe,
    SearchFilterConfigurationPipe,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgSelectModule,
    NoopAnimationsModule,
    NgMultiSelectDropDownModule.forRoot(),
    CommonModule,
    ConfirmationPopoverModule.forRoot({​​​ confirmButtonType:'danger' }​​​)
    // WjGridModule, WjGridFilterModule, WjInputModule, WjGridDetailModule

  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: HttpIntercepterService, multi: true }, UserPassingService],
  bootstrap: [AppComponent]
})
export class AppModule { }

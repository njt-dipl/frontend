import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from '../../service/register.service';
import { DatePipe } from '@angular/common';
import { MessageService } from '../../service/message.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [DatePipe]
})
export class RegisterComponent implements OnInit {

  username = ''
  password = ''
  firstname = ''
  lastname = ''
  email = ''
  verified = true
  creationDate = new Date()
  constructor(private router: Router,
    private registerService: RegisterService,
    private messageService: MessageService) {
  }
  ngOnInit(): void {
  }

  saveUser() {

    this.registerService.saveUser(this.username, this.password, this.firstname, this.lastname, this.email, this.creationDate)
      .subscribe(
        data => {
          console.log(data)
          // this.messageService.sendMail(this.email, this.username, this.password).subscribe(
          //   data => {
          //     console.log(data)
          // }
          // )
        },
        error => {
          console.log(error)
        }
      )
      this.router.navigate(['login'])
      alert("Proverite Vaš mejl")
  }
}

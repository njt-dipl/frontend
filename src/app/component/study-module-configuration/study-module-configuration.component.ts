import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FacultyDataService } from 'src/app/service/data/faculty-data.service';
import { SemesterConfigurationService } from 'src/app/service/data/semester-configuration.service';
import { SubjectDataService } from 'src/app/service/data/subject-data.service';
import { StudyModule, StudyProgram } from '../faculty/faculty.component';
import { Subject, SubjectType } from '../subject/subject.component';

export class SemesterConfiguration {
  idSemesterConfiguration: number
  idStudyProgram: number
  position: number
  semester: number
  idSubjectType: number
  espb: number
  mandatory: String
  idStudyModule: number
  status: boolean
  subjectTypeName: String
  subjects: Array<Subject>
  subjectsIds: number[]
  // idSubject: number

  constructor(
    idSemesterConfiguration: number,
    idStudyProgram: number,
    position: number,
    semester: number,
    idSubjectType: number,
    espb: number,
    mandatory: String,
    idStudyModule: number,
    status: boolean,
    subjectTypeName: String,
    // idSubject: number
    subjects: Array<Subject>,
    subjectsIds: number[]
  ) {
    this.idSemesterConfiguration = idSemesterConfiguration;
    this.idStudyProgram = idStudyProgram;
    this.position = position
    this.semester = semester
    this.idSubjectType = idSubjectType
    this.espb = espb
    this.mandatory = mandatory
    this.idStudyModule = idStudyModule
    this.status = status
    this.subjectTypeName = subjectTypeName
    // this.idSubject = idSubject
    this.subjects = subjects
    this.subjectsIds = subjectsIds
  }
}
@Component({
  selector: 'app-study-module-configuration',
  templateUrl: './study-module-configuration.component.html',
  styleUrls: ['./study-module-configuration.component.css']
})
export class StudyModuleConfigurationComponent implements OnInit {

  popoverProgramMessageSave = 'Ukoliko su pozicije konfiguracija na modulima pod ovim programom vec zauzete, one će biti zamenjene gore unetim. Da li želite da nastavite?'
  popoverTitleSave: string = 'Primena konfiguracije'
  popoverTitle: string = 'Brisanje'
  popoverProgramMessage: string = 'Brisanjem konfiguracije na nivou studijskog programa biće obrisane i konfiguracije na studijskim modulima pod ovim programom. Da li želite da nastavite?'
  popoverModuleMessage: string = 'Da li ste sigurni da želite da obrišete izabranu konfiguraciju za modul ?'
  confirmClicked: boolean = false
  cancelClicked: boolean = false
  studyModule: StudyModule = new StudyModule(-1, '', '', -1, false, -1, -1)
  studyModuleId: number = -1
  semesterConfigurationList: SemesterConfiguration[] = [];
  semesterConfiguration: SemesterConfiguration = new SemesterConfiguration(-1, this.studyModule.idStudyProgram, 1, 1, -1, 0, '', this.studyModuleId, true, '', [],[])
  numberOfRows: number = 0
  showRow: boolean = true
  tableView: boolean = false
  buttonView: boolean = false
  numberOfSemester: number = 0
  semesters: number[] = []
  dalje: boolean = true
  dodaj: boolean = true
  subjectTypes: SubjectType[] = []
  position: number = 0
  mandatories: String[] = []
  brojac: number = 0;
  studyModuleSP: StudyModule = new StudyModule(-1, '', '', -1, true, -1, -1)
  // types: Array<string> = ["AO", "TM", "NS", "SA"]
  constructor(
    private route: ActivatedRoute,
    private facultyService: FacultyDataService,
    private service: SemesterConfigurationService,
    private router: Router,
    private subjectService: SubjectDataService
  ) {

  }
  ngOnInit(): void {
    // this.brojac = 0;
    this.studyModuleId = this.route.snapshot.params['id']
    // this.getSemesterConfigurationListByStudyModule(this.studyModuleId)
    if (this.studyModuleId != -1) {
      this.facultyService.getStudyModule(this.studyModuleId).subscribe(
        data =>{
          this.studyModule = data;
          this.goToSPConfig(this.studyModule.idStudyProgram)
        }

      )
    }
    console.log(this.studyModuleId)
    this.getAllSubjectTypes()
    this.getMandatories()
  }

  // getSemesterConfigurationListByStudyModule(idStudyModule: number) {

  //   this.service.getSemesterConfigurationListByStudyModule(idStudyModule).subscribe(
  //     data => {
  //       this.semesterConfigurationList = data
  //       console.log(data)
  //       // this.refreshPosition();

  //     },
  //     error => {
  //       console.log(error)
  //     }
  //   )
  //   // this.brojac = 0;

  // }



  addFieldValue(sem: number) {
    // let array = [2, 3, 4, 5, 6, 7, 8];
    this.semesterConfiguration.idStudyModule = this.studyModuleId;
    // for (let i = 0; i < this.semesterConfigurationList.length; i++) {
    //   this.semesterConfiguration = this.semesterConfigurationList[i]
    // }
    this.position = this.semesterConfigurationList.length
    this.semesterConfiguration = new SemesterConfiguration(-1, this.studyModule.idStudyProgram, ++this.position, sem, -1, 0, '', this.studyModuleId, true, '', [],[])
    this.semesterConfigurationList.push(this.semesterConfiguration)

    console.log(this.semesterConfigurationList)
    this.dodaj = false
    // this.showRow=true
  }

  deleteFieldValue(index: number, idSemesterConfiguration: number) {
    this.semesterConfigurationList.splice(index, 1);
    this.service.deleteConfiguration(idSemesterConfiguration)
      .subscribe(
        data =>
          console.log(data)
      )
    this.refreshPosition();

    // this.showRow = true
  }

  refreshPosition() {
    this.brojac = 0;
    this.semesterConfigurationList.forEach(element => {
      this.brojac++;
      element.position = this.brojac;
    });
    // this.service.refreshPositions()
  }

  saveConfiguration(numberOfSemester: number) {

    console.log(this.semesterConfigurationList)
    for (let i = 0; i < this.semesterConfigurationList.length; i++) {
      console.log(this.semesterConfigurationList[i])
      this.service.saveConfiguration(this.semesterConfigurationList[i].idSemesterConfiguration, this.semesterConfigurationList[i].idStudyProgram, this.semesterConfigurationList[i].espb,
        this.semesterConfigurationList[i].position, numberOfSemester, this.semesterConfigurationList[i].idSubjectType,
        this.semesterConfigurationList[i].mandatory, this.semesterConfigurationList[i].idStudyModule, this.semesterConfigurationList[i].status, this.semesterConfigurationList[i].subjectTypeName).subscribe(
          data => {
            console.log(data)

          },
          error => {
            console.log(error)
          }
        )
    }
    this.dalje = true
    this.semesterConfigurationList = []
    // this.semesterConfiguration.position = 1
    this.numberOfSemester = 0
  }
  formArray() {

    this.buttonView = true
    let i = 0
    while (i < 1) {
      console.log('usao u while 2')
      this.semesterConfiguration = new SemesterConfiguration(-1, this.studyModule.idStudyProgram, 0, 0, -1, 0, '', this.studyModuleId, true, '', [],[])
      i++
    }
    this.tableView = true
    this.showRow = false


  }
  formTable() {

    this.service.getSemesterConfigurationListByStudyModuleAndSemester(this.studyModule.idStudyProgram, this.studyModuleId, this.numberOfSemester).subscribe(
      data => this.semesterConfigurationList = data
    )

    console.log("lista " + this.semesterConfigurationList)
    this.tableView = true
    this.dalje = false
    // this.buttonView = false
    let number = 2
    // let i = 0
    console.log('semesters ' + this.semesters)
    this.semesterConfiguration.idStudyModule = this.studyModuleId;
    console.log('usao u for')
    console.log('numberOfSemesters : ' + this.numberOfSemester)

    for (let i = 0; i < this.semesterConfigurationList.length; i++) {
      this.semesterConfiguration = this.semesterConfigurationList[i]
    }
    // while (i < 1) {
    //   console.log('usao u while 2')
    //   this.semesterConfigurationList.push(this.semesterConfiguration)
    //   this.semesterConfiguration = new SemesterConfiguration(number++, this.numberOfSemester, -1, 0, '', this.studyModuleId)
    //   i++
    // }
    this.tableView = true

    console.log(this.semesterConfigurationList)
    this.showRow = false
    // this.refreshPosition()
  }
  counter(i: number) {
    return new Array(i);
  }

  setSemester(semester: number) {
    this.semesterConfiguration.semester = semester
    // this.semesterConfigurationList.push(this.semesterConfiguration)
  }

  getAllSubjectTypes() {
    this.subjectService.getSubjectTypes().subscribe(
      response => {
        console.log(response);
        this.subjectTypes = response;
        console.log(this.subjectTypes);
        // this.mapST = response;
      }
    )
  }
  refresh() {
    this.numberOfSemester = 0
    this.dalje = true

  }

  getMandatories() {
    this.service.getMandatories().subscribe(
      response => {
        console.log(response);
        this.mandatories = response;

      }
    )
  }

  goToSPConfig(idStudyProgram: number) {
    this.facultyService.getIdModuleForSP(idStudyProgram).subscribe(
      data => {
        console.log("prvo: " + data)

        this.studyModuleSP = data;
        console.log("STUDIJSKI PROG U MODUL: " + this.studyModuleSP.idStudyModule)
      })
  }
}
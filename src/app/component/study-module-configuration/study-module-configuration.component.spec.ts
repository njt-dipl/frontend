import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyModuleConfigurationComponent } from './study-module-configuration.component';

describe('StudyModuleConfigurationComponent', () => {
  let component: StudyModuleConfigurationComponent;
  let fixture: ComponentFixture<StudyModuleConfigurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudyModuleConfigurationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyModuleConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

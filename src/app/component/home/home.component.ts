import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Router } from '@angular/router';
import { Faculty, FacultyGrouping, University } from '../faculty/faculty.component';
import { FacultyDataService } from '../../service/data/faculty-data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  universities: University[] = [];
  facultyGroupings: FacultyGrouping[] = [];
  faculties: Faculty[] = [];

  constructor(
    public router: Router,
    private facultyService: FacultyDataService
    ) {
  }


  ngOnInit(): void {
    this.refreshView();
  }

  addItem() {
    console.log('radi')
  }
  refreshView() {
    this.facultyService.getAllUniversities().subscribe(
      response => {
        this.universities = response;
        console.log(this.universities);
      });
    this.facultyService.getAllFacultyGroupings().subscribe(
      response => {
        this.facultyGroupings = response;
        console.log(this.facultyGroupings)
      });
    this.facultyService.getAllFaculties().subscribe(
      response => {
        this.faculties = response;
        console.log(this.faculties)
      });
  }

  isInGroup(university: University, facultyGrouping: FacultyGrouping, faculty: Faculty): boolean {
    if (faculty.facultyGroupingId === facultyGrouping.idGrouping && faculty.universityId === university.idUniversity) {
      return true;
    }
    else {
      return false;
    }
  }

  handleFaculty(faculty: Faculty){
    this.router.navigate(['faculty', faculty.id])
  }

}
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from 'src/app/service/register.service';
import { UserPassingService } from 'src/app/service/user-passing.service';
import { User } from '../login/login.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  json: User = new User(-1, 'first', 'user', 'user', 'anja', '');

  constructor(public userPassService: UserPassingService,
    public registerService: RegisterService,
    public router: Router) {
    console.log('pozvala sam konstruktor')
  }

  ngOnInit(): void {


    console.log("SETTINGS ON INIT: ", this.json)
    this.json = JSON.parse(localStorage.getItem("loggedUser") ?? '');
    this.json.password = ""
    console.log("USER:", this.json);


    // console.log('pozvala sam onINit', this.userLoggedIn)

    // this.userPassService.userLoggedIn.subscribe(

    //   data => {
    //     console.log('u servisu sam')

    //     console.log("SETIINGS PRE: ", data)
    //     this.userLoggedIn = data;
    //     console.log("SETTINGS USLA SAM U EVENT EMITTER ", this.userLoggedIn);
    //   }
    // )

  }

  updateUser() {
    this.userPassService.update(this.json)
      .subscribe(
        data => {
          console.log(data)
          localStorage.setItem('loggedUser', JSON.stringify(data))
          this.router.navigate(['home'])

        }
      )

  }
}

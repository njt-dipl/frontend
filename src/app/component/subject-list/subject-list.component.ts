import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FacultyDataService } from 'src/app/service/data/faculty-data.service';
import { SubjectDataService } from '../../service/data/subject-data.service';
import { Faculty } from '../faculty/faculty.component';
import { FundOfClasses } from '../fund-of-classes/fund-of-classes.component';
import { Subject, SubjectType } from '../subject/subject.component';

@Component({
  selector: 'app-subject-list',
  templateUrl: './subject-list.component.html',
  styleUrls: ['./subject-list.component.css']
})
export class SubjectListComponent implements OnInit {

  facultyId: number = -1
  faculty: Faculty = new Faculty(-1, '', '', '', '', -1, -1, -1)
  subjects: Subject[] = []
  subjectTypes: SubjectType[] = []
  sn: string = ''
  fundOfClasses: FundOfClasses[] = []
  fundString: string = ''
  searchValue: string = ''
  popoverTitle: string = 'Brisanje'
  popoverMessage: string = 'Da li ste sigurni da želite da obrišete izabrani predmet?'
  confirmClicked: boolean = false
  cancelClicked: boolean = false

  constructor(
    private route: ActivatedRoute,
    private subjectService: SubjectDataService,
    private facultyService: FacultyDataService,
    public router: Router) { }

  ngOnInit(): void {
    this.facultyId = this.route.snapshot.params['idFaculty']

    if (this.facultyId != -1) {
      this.facultyService.getFaculty(this.facultyId).subscribe(
        data => this.faculty = data
      )
    }

    // this.getSubjects()
    this.refreshSubjects()
    console.log(this.subjects)
    this.getSubjectTypes();
    
  }

  getSubjects() {
    this.subjectService.getSubjectsForFaculty(this.facultyId).subscribe(
      data => this.subjects = data    
    )
    console.log(this.subjects)
  }

  addSubject() {
    this.router.navigate(['subject/getAll', this.facultyId, -1])
  }

  updateSubject(subjectId: number) {
    console.log(`update subject ${subjectId}`)
    this.router.navigate(['subject/getAll', this.facultyId, subjectId])
  }

  deleteSubject(subjectId: number) {
    console.log(`delete subject ${subjectId}`)
    this.subjectService.deleteSubject(subjectId).subscribe(
      response => {
        console.log(response);
        // this.message = `Deleting of subject No ${subjectId} is successful!`
        this.refreshSubjects()
        // this.getAllFunds()
      }
    )
  }


  refreshSubjects() {
    this.subjectService.getSubjectsForFaculty(this.facultyId).subscribe(
      response => {
        this.subjects = response;
        this.subjectService.getFunds().subscribe(
          response => {
            this.fundOfClasses = response;
          }
        )
      }
    )
    // this.getAllFunds()
  }

  getSubjectTypes() {
    this.subjectService.getSubjectTypes().subscribe(
      response => {
        this.subjectTypes = response;
      }
    )
  }

  getShortNameOfType(id: number) {
      this.subjectTypes.forEach(element => {
        if(element.id==id) this.sn =  element.shortenedName;
      });
  }


  getAllFunds(){
    this.subjectService.getFunds().subscribe(
      response => {
        this.fundOfClasses = response;
      }
    )
  }

  addFund(idSubject: number){
    this.fundString=''
    // this.router.navigate(['fund_of_classes/', this.facultyId, idSubject])
    this.fundOfClasses.forEach(element => {
      if(element.subjectId == idSubject){
        if(this.fundString===''){
        this.fundString = this.fundString+element.numberOfClasses
        } else {
          this.fundString = this.fundString+'+'+element.numberOfClasses
        }
      }
    });
    // console.log(this.fundString)    
  }

}

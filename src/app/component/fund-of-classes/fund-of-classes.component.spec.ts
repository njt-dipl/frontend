import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FundOfClassesComponent } from './fund-of-classes.component';

describe('FundOfClassesComponent', () => {
  let component: FundOfClassesComponent;
  let fixture: ComponentFixture<FundOfClassesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FundOfClassesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FundOfClassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

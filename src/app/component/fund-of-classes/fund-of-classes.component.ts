import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SubjectDataService } from 'src/app/service/data/subject-data.service';
import { Subject } from '../subject/subject.component';

export class FundOfClasses {
  subjectId: number
  teachingFormId: number
  teachingFormName: string
  numberOfClasses: number
  constructor(
    subjectId: number,
    teachingFormId: number,
    teachingFormName: string,
    numberOfClasses: number
  ) {
    this.subjectId = subjectId;
    this.teachingFormId = teachingFormId;
    this.teachingFormName = teachingFormName;
    this.numberOfClasses = numberOfClasses;
  }
}

export class TeachingForm {
  idTeachingForm: number
  name: string
  shortenedName: string
  constructor(
    idTeachingForm: number,
    name: string,
    shortenedName: string
  ) {
    this.idTeachingForm = idTeachingForm;
    this.name = name;
    this.shortenedName = shortenedName;
  }
}

@Component({
  selector: 'app-fund-of-classes',
  templateUrl: './fund-of-classes.component.html',
  styleUrls: ['./fund-of-classes.component.css']
})
export class FundOfClassesComponent implements OnInit {

  teachingForms: TeachingForm[] = []
  subjectId: number = -1
  facultyId: number = -1
  subject: Subject = new Subject(-1, '', '', -1, -1, -1)
  // fundOfClasses: FundOfClasses = new FundOfClasses(-1, -1, -1)
  fundsOfClasses: FundOfClasses[] = []


  constructor(
    private route: ActivatedRoute,
    private subjectService: SubjectDataService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.facultyId = this.route.snapshot.params['idFaculty']
    this.subjectId = this.route.snapshot.params['idSubject']
    this.getAllTeachingForms()

    if (this.subjectId != -1) {
      this.subjectService.getSubject(this.facultyId, this.subjectId).subscribe(
        data => this.subject = data
      )
    }

  }

  getAllTeachingForms() {
    this.subjectService.getTeachingForms().subscribe(
      response => {
        console.log(response);
        this.teachingForms = response;

      }
    )
  }

  addNewFundToList(teachingFormId: number) {
    // this.fundsOfClasses.push(new FundOfClasses(this.subjectId, teachingFormId, 0))
  }

  addFundsFinally() {
    for(let item of this.fundsOfClasses){
    // this.fundsOfClasses.forEach(element => {
      console.log('ELEMENT '+item)
      this.subjectService.createClassesFund(item.subjectId, item.teachingFormId, item.numberOfClasses)
        .subscribe(
          data => {
            console.log(data)
          }
        )}
        // );
    // }
    this.router.navigate(['subject/getAll/', this.facultyId])
  }
}

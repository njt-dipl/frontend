import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Event, Router } from '@angular/router';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { FacultyDataService } from 'src/app/service/data/faculty-data.service';
import { FillModuleConfigurationService } from 'src/app/service/data/fill-module-configuration.service';
import { SemesterConfigurationService } from 'src/app/service/data/semester-configuration.service';
import { SubjectDataService } from 'src/app/service/data/subject-data.service';
import { StudyModule } from '../faculty/faculty.component';
import { FundOfClasses } from '../fund-of-classes/fund-of-classes.component';
import { SemesterConfiguration } from '../study-module-configuration/study-module-configuration.component';
import { Subject, SubjectComponent, SubjectType } from '../subject/subject.component';


export class SubjectConfiguration {
  idSubjectConfiguration: number
  idSemesterConfiguration: number
  // subjectsId: number[]
  // idStudyProgram: number
  subjects: Subject[]

  // idStudyModule: number
  // semester: number
  // position: number
  // status: boolean
  // idSubjectType: number
  // espb: number
  // mandatory: String

  constructor(
    idSubjectConfiguration: number,
    idSemesterConfiguration: number,
    // subjectsId: number[]

    // idStudyProgram: number,
    subjects: Subject[]

    // idStudyModule: number,
    // semester: number,
    // position: number,
    // idSubjectType: number,
    // espb: number,
    // mandatory: String,
    // status: boolean
  ) {
    this.idSubjectConfiguration = idSubjectConfiguration;
    this.idSemesterConfiguration = idSemesterConfiguration;
    // this.subjectsId = subjectsId

    // this.idStudyProgram = idStudyProgram;
    this.subjects = subjects;
    // this.idStudyModule = idStudyModule;
    // this.semester = semester;
    // this.position = position;
    // this.status = status;
    // this.idSubjectType = idSubjectType;
    // this.espb = espb;
    // this.mandatory = mandatory;
  }
}

@Component({
  selector: 'app-fill-module-configuration',
  templateUrl: './fill-module-configuration.component.html',
  styleUrls: ['./fill-module-configuration.component.css']
})
export class FillModuleConfigurationComponent implements OnInit {

  obavezan: string = "obavezan"
  studyModuleId: number = 0
  studyModule: StudyModule = new StudyModule(-1, '', '', -1, false, -1, -1)
  facultyId: number = 0
  semesterConfigurationList: SemesterConfiguration[] = []
  subjectTypes: SubjectType[] = []
  fulln: string = ''
  subjects: Subject[] = []
  subjectConfiguration = new SubjectConfiguration(-1, -1, [])
  subjectConfigurationList: SubjectConfiguration[] = []
  listaIzbornih: Subject[] = []
  listaAlternativnih: Subject[] = []
  listaObaveznih: Subject[] = []
  dalje: boolean = true
  semesters: number[] = []
  tableView: boolean = false
  numberOfSemester: number = 0
  showRow: boolean = true
  semesterConfiguration: SemesterConfiguration = new SemesterConfiguration(-1, this.studyModule.idStudyProgram, 1, 1, -1, 0, '', this.studyModuleId, true, '', [], [])
  studyModuleSP: StudyModule = new StudyModule(-1, '', '', -1, true, -1, -1)

  mySelections = [3];

  dropdownSettings: IDropdownSettings = {};
  events: Event[] = [];


  // subjectConf: SubjectConfiguration = new SubjectConfiguration(-1, -1, -1, -1, -1)

  fundString: string = ''
  subject: Subject = new Subject(-1, '', '', -1, -1, this.facultyId)
  fundOfClasses: FundOfClasses[] = []
  idSubject: number = 0

  searchValue: string = ''

  i: number = 0
  j: number = 0
  selectedAttributes = []
  subjectsSel = []
  // subject$ = 


  constructor(private http: HttpClient,
    private route: ActivatedRoute,
    private service: SemesterConfigurationService,
    private facultyService: FacultyDataService,
    private subjectService: SubjectDataService,
    private subjectConfigurationService: FillModuleConfigurationService,
    public router: Router) { }

  ngOnInit(): void {
    this.i = 0
    this.subjectConfigurationList = []
    this.facultyId = this.route.snapshot.params['idFaculty']
    this.studyModuleId = this.route.snapshot.params['idStudyModule']
    if (this.studyModuleId != -1) {
      this.facultyService.getStudyModule(this.studyModuleId).subscribe(
        data => {
          this.studyModule = data
          this.goToSPConfig(this.studyModule.idStudyProgram)
        }
      )
    }

    //ovde bi valjda trebalo da uzimam SubjectConfigurationListPoModulu
    // this.getSubjectConfigurationListByStudyModule(this.studyModuleId)
    // this.getSemesterConfigurationListByStudyModule(this.studyModuleId)

    this.getSubjectTypes();
    this.getAllSubjects();

    // this.subjectService.getFunds().subscribe(
    //   response => {
    //     this.fundOfClasses = response;
    //   }
    // )
    // this.dropdownSettings = {
    //   idField: 'item_id',
    //   textField: 'item_text',
    // };


  }

  compareFn(item: Subject, selected: Subject) {
    return item.idSubject === selected.idSubject;
  }

  // selectDefault(config: SemesterConfiguration) {
  //   var index = this.subjects.findIndex(x => x.idSubject == 1);
  //   //this will set value
  //   config.subjects = this.subjects[index].idSubject;
  // }

  getSemesterConfigurationListByStudyModule(idStudyModule: number) {
    this.service.getSemesterConfigurationListByStudyModule(this.studyModule.idStudyProgram, idStudyModule, this.numberOfSemester).subscribe(
      data => {
        this.semesterConfigurationList = data
        console.log(data)

        this.semesterConfigurationList.forEach(element => {
          console.log("SEM KONFIG LIST:", element)
          this.addSubjectConfigurationInList(element)
        });

      },
      error => {
        console.log(error)
      }
    )
    this.semesterConfigurationList.forEach(element => {
      element.subjects.push(new Subject(-1, '', '', -1, 0, -1))
    });
  }

  // getSubjectConfigurationListByStudyModule(idStudyModule: number) {
  //   this.service.getSubjectConfigurationListByStudyModule(idStudyModule).subscribe(
  //     data => {
  //       this.subjectConfigurationList = data
  //       console.log(data)
  //     }
  //   )
  // }

  getSubjectTypes() {
    this.subjectService.getSubjectTypes().subscribe(
      response => {
        console.log(response);
        this.subjectTypes = response;
        console.log(this.subjectTypes);
        // this.mapST = response;
      }
    )
  }

  getFullNameOfType(id: number) {
    this.subjectTypes.forEach(element => {
      if (element.id == id) this.fulln = element.fullName;
    });
  }

  getAllSubjects() {
    this.subjectService.getSubjectsForFaculty(this.facultyId).subscribe(
      response => {
        console.log(response)
        this.subjects = response;
      }
    )
  }

  // getSubjectOfType(idType: number){
  //   this.subjectService.getSubjectOfType(idType).subscribe(
  //     response => {
  //       console.log(response)
  //       this.subjects = response;
  //     }
  //   )
  // }

  addFund(idSubject: number) {
    this.fundString = ''
    // this.router.navigate(['fund_of_classes/', this.facultyId, idSubject])
    this.fundOfClasses.forEach(element => {
      if (element.subjectId == idSubject) {
        if (this.fundString == '') {
          this.fundString = this.fundString + element.numberOfClasses
        } else {
          this.fundString = this.fundString + '+' + element.numberOfClasses
        }
      }
    });
    // console.log(this.fundString)    
  }


  addSubjectConfigurationInList(conf: SemesterConfiguration) {
    this.i = this.i + 1
    // this.j = this.i-1
    this.subjectConfiguration = new SubjectConfiguration(-1, conf.idSemesterConfiguration, conf.subjects),
      this.subjectConfigurationList.push(this.subjectConfiguration)
    // console.log('i:'+ this.i)
    console.log('lista iz baze:', this.subjectConfigurationList.length)
  }




  generateSubjectConfigurations() {

    console.log("***LISTA: ", this.semesterConfigurationList)
    // console.log("*************************", this.semesterConfiguration)
    this.semesterConfigurationList.forEach(element => {
      let nizSubjects: Subject[] = []
      let niz: number[] = []
      console.log("ELEMENT config", element)

      // element.subjects.forEach(subjectt => { console.log('predmet ' + subjectt.name) })
      element.subjects.forEach(element2 => {
        niz.push(element2.idSubject)
        // console.log("niz:",niz)
        nizSubjects.push(element2);
        console.log("ELEMENT subject: ", element2)


        this.subjectConfiguration = new SubjectConfiguration(-1, element.idSemesterConfiguration, nizSubjects)
        // console.log('conf ', this.subjectConfiguration.subjects)
        this.subjectConfigurationService.saveSubjectConfiguration(this.subjectConfiguration).subscribe(

          data => {
            console.log("SACUVAO: ", data)
          },
          error => {
            console.log(error)
          }
        )
      });
    })
    this.router.navigate(['faculty', this.facultyId])
  }

  // izborniPredmeti(conf: SemesterConfiguration) {
  //   this.subjects.forEach(element => {
  //     if (element.espb == conf.espb && element.idSubjectType == conf.idSubjectType)
  //       this.listaIzbornih.push(element);
  //   })

  //   return this.listaIzbornih;
  // }

  // alternativniPredmeti(conf: SemesterConfiguration) {
  //   this.subjects.forEach(element => {
  //     if (element.espb == conf.espb && element.idSubjectType == conf.idSubjectType)
  //       this.listaAlternativnih.push(element);
  //   })

  //   return this.listaAlternativnih;
  // }
  // obavezniPredmeti(conf: SemesterConfiguration) {
  //   this.subjects.forEach(element => {
  //     if (element.espb == conf.espb && element.idSubjectType == conf.idSubjectType)
  //       this.listaObaveznih.push(element);
  //   })


  //   return this.listaObaveznih;
  // }

  formTable() {

    this.service.getSemesterConfigurationListByStudyModule(this.studyModule.idStudyProgram, this.studyModuleId, this.numberOfSemester).subscribe(

      data => {
        this.semesterConfigurationList = data,
          console.log("lista ", this.semesterConfigurationList)
      }
    )

    // this.semesterConfigurationList.forEach(element => {
    //   console.log
    // });
    this.tableView = true
    this.dalje = false
    // this.buttonView = false
    let number = 2
    // let i = 0
    console.log('semesters ', this.semesters)
    this.semesterConfiguration.idStudyModule = this.studyModuleId;
    console.log('numberOfSemesters : ', this.numberOfSemester)

    for (let i = 0; i < this.semesterConfigurationList.length; i++) {
      this.semesterConfiguration = this.semesterConfigurationList[i]
    }
    // while (i < 1) {
    //   console.log('usao u while 2')
    //   this.semesterConfigurationList.push(this.semesterConfiguration)
    //   this.semesterConfiguration = new SemesterConfiguration(number++, this.numberOfSemester, -1, 0, '', this.studyModuleId)
    //   i++
    // }
    this.tableView = true

    console.log(this.semesterConfigurationList)
    this.showRow = false
    // this.refreshPosition()
  }

  goToSPConfig(idStudyProgram: number) {
    this.facultyService.getIdModuleForSP(idStudyProgram).subscribe(
      data => {
        console.log("prvo: " + data)

        this.studyModuleSP = data;
        console.log("STUDIJSKI PROG U MODUL: " + this.studyModuleSP.idStudyModule)
      })
  }

  findItems(conf: SemesterConfiguration) {
    let items: Subject[] = []
    this.subjects.forEach(element => {
      if (element.idSubjectType == conf.idSubjectType && element.espb == conf.espb) {
        items.push(element);
      }
    });
    return items;
  }

}
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillModuleConfigurationComponent } from './fill-module-configuration.component';

describe('FillModuleConfigurationComponent', () => {
  let component: FillModuleConfigurationComponent;
  let fixture: ComponentFixture<FillModuleConfigurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillModuleConfigurationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FillModuleConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SubjectDataService } from '../../service/data/subject-data.service';
import { FundOfClasses, TeachingForm } from '../fund-of-classes/fund-of-classes.component';

export class SubjectType {
  id: number
  fullName: string
  shortenedName: string
  constructor(
    id: number,
    fullName: string,
    shortenedName: string) {
    this.id = id;
    this.fullName = fullName;
    this.shortenedName = shortenedName;
  }
}

export class Subject {
  idSubject: number
  name: string
  shortenedName: string
  idSubjectType: number
  espb: number
  facultyId: number
  constructor(
    idSubject: number,
    name: string,
    shortenedName: string,
    idSubjectType: number,
    espb: number,
    facultyId: number,
  ) {
    this.idSubject = idSubject;
    this.name = name;
    this.shortenedName = shortenedName;
    this.idSubjectType = idSubjectType;
    this.espb = espb;
    this.facultyId = facultyId;
  }
}


@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.css']
})
export class SubjectComponent implements OnInit {

  subjectType: SubjectType = new SubjectType(-1, 'puni', 'skraceni')
  subjectId: number = -1
  subject = new Subject(this.subjectId, 'name', 'sn', this.subjectType.id, 1, -1)
  facultyId: number = -1
  subjectTypes: SubjectType[] = []
  teachingForms: TeachingForm[] = []
  fundsOfClasses: FundOfClasses[] = []


  numberOfClasses: number = 0
  position = 0

  teachingFormId: number = -1
  fund1 = new FundOfClasses(-1, -1, '', 0)

  constructor(
    private route: ActivatedRoute,
    private subjectService: SubjectDataService,
    private router: Router) { }

  ngOnInit(): void {
    this.subjectId = this.route.snapshot.params['idSubject']
    this.facultyId = this.route.snapshot.params['idFaculty']
    this.getSubjectTypes();
    this.subject = new Subject(this.subject.idSubject, '', '', this.subjectType.id, 1, this.facultyId)
    if (this.subjectId != -1) {
      this.subjectService.getSubject(this.facultyId, this.subjectId).subscribe(
        data => {
          this.subject = data

          this.subjectService.getFundsOfSubject(this.subject.idSubject).subscribe(
            data => {
              this.fundsOfClasses = data
              console.log("LENGTH: " + this.fundsOfClasses.length)
              this.fundsOfClasses.forEach(element => {
                // this.fund1 = element;
                console.log("fondovi predmeta: " + element.teachingFormId + " " + element.teachingFormName)

                this.spliceSelectedTFFromFirstCB(element);
              });
            }
          )
        }
      )
      this.fund1 = new FundOfClasses(-1, -1, '', 0)

    } else {
      this.subject.idSubjectType=1
      // this.fundsOfClasses.push(this.fund1)
    }


    this.getTeachingForms();

    console.log("FONDOVI na pocetku: " + this.fundsOfClasses)
    this.fundsOfClasses.forEach((element: any) => {
      console.log(element)

    });

  }

  saveSubject() {
    // this.subject.classesFund = this.fundOfClasses.theoretical + '+' + this.fundOfClasses.exercises + '+' + this.fundOfClasses.lab
    // if (this.subjectId == -1) {
    //kreiram novi subject 
    this.subjectService.createSubject(this.subject)
      .subscribe(
        data => {
          console.log(data)
          this.subject = data

          this.fundsOfClasses.forEach(element => {
            console.log('ELEMENT ' + element)
            this.subjectService.createClassesFund(this.subject.idSubject, element.teachingFormId, element.numberOfClasses)
              .subscribe(
                data => {
                  console.log(data)
                }
              )
          });


          this.router.navigate(['subject/getAll/', this.facultyId])
        }
      )

    // this.router.navigate(['subject/getAll/', this.facultyId])
    // } else {
    //   this.subjectService.updateSubject(this.subjectId, this.subject)
    //     .subscribe(
    //       data => {
    //         console.log(data)
    //         this.router.navigate(['subject/getAll/', this.facultyId])
    //       }
    //     )
    // }
  }

  getSubjectTypes() {
    this.subjectService.getSubjectTypes().subscribe(
      response => {
        // console.log(response);
        this.subjectTypes = response;
        // console.log(this.subjectTypes);
      }
    )
  }

  getTeachingForms() {
    this.subjectService.getTeachingForms().subscribe(
      response => {
        this.teachingForms = response;
      }
    )
  }

  // addNewFundToList(element: TeachingForm) {
  //   this.fundsOfClasses.push(new FundOfClasses(this.subjectId, element.idTeachingForm, element.shortenedName, 0))
  // }

  findTFName() {
    this.teachingForms.forEach(element => {
      if (element.idTeachingForm == this.fund1.teachingFormId) {
        this.fund1.teachingFormName = element.name;
        return
      }
    });
  }

  addFieldValue() {
    console.log("FUND 1 pre find: " + this.fund1.teachingFormId + " " + this.fund1.teachingFormName)

    this.findTFName()
    let fc: FundOfClasses = new FundOfClasses(this.subjectId, this.fund1.teachingFormId, this.fund1.teachingFormName, this.fund1.numberOfClasses)
    console.log("LENGTH pre push: " + this.fundsOfClasses.length)
    console.log("FUND 1 pre push: " + this.fund1.teachingFormId + " " + this.fund1.teachingFormName)

    this.fundsOfClasses.push(fc)

    console.log("LENGTH posle push : " + this.fundsOfClasses.length)

    console.log("FONDOVI nakon dodaj" + this.fundsOfClasses)
    // this.position++

    this.fundsOfClasses.forEach(element => {
      console.log(element)
    });

    this.spliceSelectedTFFromFirstCB(fc);


  }

  spliceSelectedTFFromFirstCB(f: FundOfClasses) {
    let i = 0;
    this.teachingForms.forEach(element => {

      if (element.idTeachingForm == f.teachingFormId) {
        this.teachingForms.splice(i, 1)
      }
      i++;
    });
  }

  deleteFund(fund: FundOfClasses, updateMode: boolean) {
    if (updateMode) {
      console.log("USAO: " + fund.subjectId + " " + fund.teachingFormId + " " + fund.teachingFormName + " " + fund.numberOfClasses)
      this.subjectService.deleteFund(fund.subjectId, fund.teachingFormId).subscribe(
        response => {
          console.log(response);
        }
      )
      console.log("Zavrsio sa servisom")
    }
    let i = 0;
    this.fundsOfClasses.forEach(element => {
      if (element.teachingFormId == fund.teachingFormId) {
        this.fundsOfClasses.splice(i, 1)
      }
      i++;
    });

    this.teachingForms.push(new TeachingForm(fund.teachingFormId, fund.teachingFormName, ''))



  }



}

import { Component, OnInit, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { UserPassingService } from 'src/app/service/user-passing.service';
import { AuthenticationService } from '../../service/authentication.service';
import { User } from '../login/login.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
userLoggedIn = new User(-1, 'first', '', '', '', '');

  constructor(
    public authenticationService: AuthenticationService,
    public userPassService: UserPassingService,
    private router: Router

  ) {

    // console.log("PRE U MENIJU SAM: " + this.userLoggedIn.firstname);


    // this.userPassService.userLoggedIn.subscribe(
    //   data => {
    //     console.log("Service: ",data)
    //     this.userLoggedIn = data
    //     console.log("USAO SAM U EVENT EMITTER ", this.userLoggedIn);

    //   }
    // )




    // this.authenticationService.userLoggedIn.subscribe(
    //   data => {
    //     this.userLoggedIn = data;
    //     console.log("U MENIJU SAM: "+ this.userLoggedIn.firstname);

    //   }
    // );
  }

  ngOnInit(): void {
    // console.log("ON INIT: "+this.userLoggedIn.firstname)
    // const json:User=JSON.parse(localStorage.getItem("loggedUser")??'');
    
    // console.log(json);
    
  }

  onSettings(){
    this.router.navigate(['/settings']);
  }


}

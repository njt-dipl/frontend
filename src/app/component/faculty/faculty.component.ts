import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FacultyDataService } from '../../service/data/faculty-data.service';

export class University {
  idUniversity: number
  name: string
  postalCode: number
  imgPath: string
  faculties: Faculty[]
  constructor(
    idUniversity: number,
    name: string,
    postalCode: number,
    imgPath: string,
    faculties: Faculty[]
  ) {
    this.idUniversity = idUniversity
    this.name = name;
    this.postalCode = postalCode;
    this.imgPath = imgPath;
    this.faculties = faculties;
  }
}

export class City {
  name: string
  postalCode: number
  country: string
  constructor(
    postalCode: number,
    name: string,
    country: string
  ) {
    this.name = name;
    this.postalCode = postalCode;
    this.country = country
  }
}

export class FacultyGrouping {
  idGrouping: number
  name: string
  faculites: Faculty[]
  constructor(
    idGrouping: number,
    name: string,
    faculties: Faculty[]
  ) {
    this.idGrouping = idGrouping
    this.name = name;
    this.faculites = faculties;
  }
}

export class Faculty {
  id: number
  name: string
  shortenedName: string
  address: string
  contact: string
  universityId: number
  facultyGroupingId: number
  postalCode: number

  constructor(
    id: number,
    name: string,
    shortenedName: string,
    address: string,
    contact: string,
    universityId: number,
    facultyGroupingId: number,
    postalCode: number
  ) {
    this.id = id;
    this.name = name;
    this.shortenedName = shortenedName;
    this.address = address;
    this.contact = contact;
    this.universityId = universityId;
    this.facultyGroupingId = facultyGroupingId;
    this.postalCode = postalCode;
  }
}

export class StudyProgram {
  idStudyProgram: number
  name: string
  shortenedName: string
  numberOfSemester: number
  levelOfStudy: number
  idFaculty: number
  constructor(
    idStudyProgram: number,
    name: string,
    shortenedName: string,
    numberOfSemester: number,
    levelOfStudy: number,
    idFaculty: number
  ) {
    this.idStudyProgram = idStudyProgram
    this.name = name;
    this.shortenedName = shortenedName;
    this.numberOfSemester = numberOfSemester;
    this.levelOfStudy = levelOfStudy;
    this.idFaculty = idFaculty
  }
}

export class StudyModule {
  idStudyModule: number
  name: string
  shortenedName: string
  startYear: number
  status: boolean
  idFaculty: number
  idStudyProgram: number
  // toggle: number
  constructor(
    idStudyModule: number,
    name: string,
    shortenedName: string,
    startYear: number,
    status: boolean,
    idFaculty: number,
    idStudyProgram: number,
    // toggle: number
  ) {
    this.idStudyModule = idStudyModule;
    this.name = name;
    this.shortenedName = shortenedName;
    this.startYear = startYear;
    this.status = status;
    this.idFaculty = idFaculty;
    this.idStudyProgram = idStudyProgram;
    // this.toggle = 0
  }
}




@Component({
  selector: 'app-faculty',
  templateUrl: './faculty.component.html',
  styleUrls: ['./faculty.component.css']
})
export class FacultyComponent implements OnInit {

  facultyId: number = -1
  groupingId: number = -1
  idUniversity: number = -1
  faculty: Faculty = new Faculty(-1, '', '', '', '', -1, -1, -1)
  // grouping: FacultyGrouping = new FacultyGrouping(-1,'', [])
  // university: University = new University(-1, '', -1, [])
  universities: University[] = []
  facultyGroupings: FacultyGrouping[] = []
  cities: City[] = []
  studyPrograms: StudyProgram[] = []
  studyModules: StudyModule[] = []
  studyModuleSP: StudyModule = new StudyModule(-1, '', '', -1, false, -1, -1)

  constructor(
    private route: ActivatedRoute,
    private facultyService: FacultyDataService,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.facultyId = this.route.snapshot.params['id']
    this.getUniversities()
    this.getFacultyGroupings()
    this.getCities()
    this.getStudyProgramsByFaculty(this.facultyId)
    this.getAllModules()

    if (this.facultyId != -1) {
      this.facultyService.getFaculty(this.facultyId).subscribe(
        data => this.faculty = data
      )
    }
  }

  getFacultyGroupings() {
    this.facultyService.getAllFacultyGroupings().subscribe(
      data => this.facultyGroupings = data
    )
  }

  getUniversities() {
    this.facultyService.getAllUniversities().subscribe(
      data => this.universities = data
    )
  }

  getCities() {
    this.facultyService.getAllCities().subscribe(
      data => this.cities = data
    )
  }

  handleFaculty() {
    this.router.navigate(['faculty_create_configuration', this.faculty.id])
  }

  createConfiguration(idStudyModule: number) {
    this.router.navigate(['study_module_create_configuration', idStudyModule])
  }
  fillInCongiguration(idStudyModule: number) {
    this.router.navigate(['fill_module_configuration', this.facultyId, idStudyModule])
  }

  getStudyProgramsByFaculty(facultyId: number) {
    this.facultyService.getStudyProgramsByFaculty(facultyId).subscribe(
      data => this.studyPrograms = data
    )
  }

  // getModules(idStudyProgram: number) {
  //   this.facultyService.getModulesByStudyProgram(idStudyProgram).subscribe(
  //     data => {
  //       this.studyModules = data
  //       console.log(data)
  //     }
  //   )
  // }

  getAllModules() {
    this.facultyService.getModules().subscribe(
      data => {
        this.studyModules = data
        console.log(data)
      }
    )
  }

  isInProgram(studyProgram: StudyProgram, studyModule: StudyModule): boolean {
    if (studyProgram.idStudyProgram === studyModule.idStudyProgram) {
      return true;
    }
    else {
      return false;
    }
  }


  // toggleOn(idStudyProgram: number) {
  //   for(let studyModule of this.allStudyModules){
  //     if(studyModule.idStudyProgram===idStudyProgram){
  //       studyModule.toggle=1;
  //     }
  //   }
  // }

  handleSubject() {
    this.router.navigate(['subject/getAll', this.faculty.id])
  }

  createProgramConfiguration(idStudyProgram: number) {
    // this.getIdModuleForSP(idStudyProgram);
    this.facultyService.getIdModuleForSP(idStudyProgram).subscribe(
      data => {
        console.log("prvo: " + data)

        this.studyModuleSP = data;
        console.log("STUDIJSKI PROG U MODUL: " + this.studyModuleSP.idStudyModule)
        this.router.navigate(['study_module_create_configuration', this.studyModuleSP.idStudyModule])
      }
    )

    // this.router.navigate(['study_program_create_configuration', idStudyProgram])
  }

  //fill_module_configuration/:idFaculty/:idStudyModule
  fillProgramConfiguration(idStudyProgram: number) {
    this.facultyService.getIdModuleForSP(idStudyProgram).subscribe(
      data => {
        console.log("prvo: " + data)

        this.studyModuleSP = data;
        console.log("STUDIJSKI PROG U MODUL: " + this.studyModuleSP.idStudyModule)
        this.router.navigate(['fill_module_configuration', this.facultyId, this.studyModuleSP.idStudyModule])
      })
    }
  
}

import { Component, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { UserPassingService } from 'src/app/service/user-passing.service';
// import { EventEmitter } from 'stream';
import { AuthenticationService } from '../../service/authentication.service';

export class User {
  id: number
  firstname: string
  lastname: string
  email: string
  username: string
  password: string
  constructor(id: number,
    firstname: string,
    lastname: string,
    email: string,
    username: string,
    password: string

  ) {
    this.id = id;
    this.firstname = firstname;
    this.lastname = lastname;
    this.email = email;
    this.username = username;
    this.password = password;
  }
}


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})



export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    public authService: AuthenticationService,
    public userPassService: UserPassingService
  ) { }

  @Output() loggedUser: User = new User(-1, '', '', '', '', '');
  username = ''
  firstname = ''
  lastname = ''
  password = ''
  // errorMessageParameters = 'Uneli ste pogrešno ime ili lozinku. Pokušajte ponovo!'
  errorMessage = ''
  invalidLogin = false
  invalidVerification = false
  // @Output() userLoggedIn = new EventEmitter<{username: string, firstname: string, lastname: string}>();


  ngOnInit(): void {
  }


  handleJWTAuthLogin() {
   
      this.authService.executeJWTAuthenticationService(this.username, this.password)
        .subscribe(
          data => {
            console.log("***", data)

            this.router.navigate(['home']) //dodaj, this.username
            this.invalidLogin = false
            localStorage.setItem('loggedUser', JSON.stringify(data.userProfileBasicDTO))

            // this.getByUsername(this.username)
            // this.userLoggedIn.emit({username: this.username, firstname: this.firstname, this.lastname = this.lastname});
            this.userPassService.emitUser(data.userProfileBasicDTO);
          },
          error => {

            
            console.log('greska : ', error.error)
            this.errorMessage = error.error;
            
            this.invalidLogin = true
            
          }

        )
    
    // this.authService.userLoggedIn.emit(this.loggedUser);

  }

  getByUsername(username: string) {
    this.authService.getUser(username).subscribe(
      data => {
        this.loggedUser = data;
        console.log(this.loggedUser.lastname)
        // sessionStorage.setItem()
      }
    )
  }

}

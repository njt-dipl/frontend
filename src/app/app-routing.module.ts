import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FacultyComponent } from './component/faculty/faculty.component';
import { HomeComponent } from './component/home/home.component';
import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';
import { RouteGuardService } from './service/route-guard.service';
import { SubjectListComponent } from './component/subject-list/subject-list.component';
import { SubjectComponent } from './component/subject/subject.component';
import { StudyModuleConfigurationComponent } from './component/study-module-configuration/study-module-configuration.component';
import { FillModuleConfigurationComponent } from './component/fill-module-configuration/fill-module-configuration.component';
import { FundOfClassesComponent } from './component/fund-of-classes/fund-of-classes.component';
import { SettingsComponent } from './component/settings/settings.component';

const routes: Routes = [
  { path: '', component: LoginComponent }, //canActivate, RouteGuardService
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [RouteGuardService] },
  { path: 'register', component: RegisterComponent },
  { path: 'faculty/:id', component: FacultyComponent, canActivate: [RouteGuardService] },
  { path: 'subject/getAll/:idFaculty/:idSubject', component: SubjectComponent, canActivate: [RouteGuardService] },
  { path: 'subject/getAll/:idFaculty', component: SubjectListComponent, canActivate: [RouteGuardService] },
  { path: 'study_module_create_configuration/:id', component: StudyModuleConfigurationComponent, canActivate: [RouteGuardService] },
  // { path: 'study_module_create_configuration/:id', component: StudyModuleConfigurationComponent, canActivate: [RouteGuardService] },
  { path: 'fill_module_configuration/:idFaculty/:idStudyModule', component: FillModuleConfigurationComponent, canActivate: [RouteGuardService] },
  { path: 'fund_of_classes/:idFaculty/:idSubject', component: FundOfClassesComponent, canActivate: [RouteGuardService] },
  { path: 'settings', component: SettingsComponent, canActivate: [RouteGuardService] }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

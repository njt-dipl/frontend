import { Pipe, PipeTransform } from '@angular/core';
import { SemesterConfiguration } from '../component/study-module-configuration/study-module-configuration.component';

@Pipe({
  name: 'searchFilterConfiguration'
})
export class SearchFilterConfigurationPipe implements PipeTransform {


  transform(configurations: SemesterConfiguration[], searchValue: any): SemesterConfiguration[] {
    if (!configurations || !searchValue) {
      return configurations;
    }
    return configurations.filter(configuration =>
      configuration.semester == searchValue)
  }


}

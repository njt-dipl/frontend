import { Pipe, PipeTransform } from '@angular/core';
import { Subject } from '../component/subject/subject.component';

@Pipe({
  name: 'searchFilterSubject'
})
export class SearchFilterSubjectPipe implements PipeTransform {

  transform(subjects: Subject[], searchValue: string): Subject[] {
    if (!subjects || !searchValue) {
      return subjects;
    }
    return subjects.filter(subject =>
      subject.name.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()))
  }

}
